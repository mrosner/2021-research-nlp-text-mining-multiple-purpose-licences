#!/usr/bin/env python

################################################################################
# Name        : nlp-multiple-purpose.py                                        #
# Author      : M.E.Rosner                                                     #
# E-Mail      : marty[at]rosner[dot]io                                         #
# Version     : 20.21.12.06                                                    #
# Copyright   : Copyright (C) 2021 M.E.Rosner; Berlin; Germany                 #
# License     : MIT                                                            #
# Description : NLP text mining multiple purpose licences                      #
################################################################################

# python3.9

def get_text_from_document(document='./document.txt'):
    """returns unfiltered text for a given text document."""
#    import codecs
#    fhd = codecs.open(document, mode = 'r')

    # encoding issues
    # https://stackoverflow.com/questions/436220/how-to-determine-the-encoding-of-text    
    
#    txt = fhd.read()
#    fhd.close()

#    with open(document, 'rb') as f:
#        b = f.read()
#    f.close()
#    txt = b.decode("utf-8")

    # see: https://stackoverflow.com/questions/42339876/error-unicodedecodeerror-utf-8-codec-cant-decode-byte-0xff-in-position-0-in 2021-11-24T18_44_29Z
    with open(document, encoding="utf8", errors='ignore') as f:
        txt = f.read()
    f.close()

    return txt

def get_tokens_from_text(text=u'Sincerly', language='english'):
    """returns an array of stemmed tokens."""
    tokens = []
    text = text.lower()

    import re
    if ( language == 'german'):
        text = re.sub('[^a-zäöüß]', ' ', text)
    else:               # temporary, for lang add case.
        text = re.sub('[^a-z]', ' ', text)

    import nltk         # use nltk
    # nltk.download('punkt')
    from nltk.tokenize import word_tokenize
    tokens = word_tokenize(text)

    from nltk.corpus import stopwords
    for word in tokens:
        if word in stopwords.words(language):
            tokens.remove(word)

    from nltk.stem.porter import PorterStemmer
    stemmer = PorterStemmer()

    for i in range(len(tokens)):
        tokens[i] = stemmer.stem(tokens[i])

    return tokens

def get_documents(path_to_docs='./*.txt'):
    """returns documents for a given path."""
    documents = []

    import glob
    files = glob.glob(path_to_docs)

    import os
    docs = [f for f in files if os.path.isfile(f)]
#   docs = (file for file in os.listdir(path_to_docs)

    for i in range(len(docs)):
        print(get_text_from_document(docs[i]))
        text = get_text_from_document(docs[i])
        tokens = get_tokens_from_text(text=text, language='english')
        documents.append(tokens)

    labels = docs
    for i in range(len(labels)):
        print(labels[i])

    return documents, labels

def get_count_vector(corpus=[], document=[]):
    """returns a count vector for given corpus and document. (obsolete)"""
    count = list(0 for i in range(len(corpus)))

    for j in range(len(corpus)):             # feature
        for k in range(len(document)):
            if ( corpus[j] == document[k] ): # word
                count[j] = count[j] + 1

    return count

def get_corpus(path_to_docs='./*.txt'):
    """return corpus for a given path."""
    documents, labels = get_documents(path_to_docs)

#   print(labels)

    from nltk.corpus import stopwords
    stop_words = stopwords.words('english')

    corpus = []
    for i in range(len(documents)):
        filtered_document = []
        for stop_word in stop_words:
            for token in documents[i]:
                if ( token != stop_word ):
                    filtered_document.append(token)
        corpus.append(' '.join(filtered_document))

    return corpus, labels

def get_X(path_to_docs='./*/*.txt', language='english'):
    """returns TFIDF document-term matrix and related labels for each vector"""
    import os
    corpus, files = get_corpus(path_to_docs=path_to_docs)

    labels = []
#   labels = [os.path.basename(label) for label in files]

    labels = []
    for index in range(len(files)):
        path = files[index]
        label = os.path.basename(path)
        prefix = str('%03i' % index)
        labels.append(prefix + '-' + label)

#   print(labels)

    from nltk.corpus import stopwords
    stop_words = stopwords.words(language)

    from sklearn.feature_extraction.text import TfidfVectorizer
    vectorizer = TfidfVectorizer(stop_words=stop_words)
    X = vectorizer.fit_transform(corpus)

#   obsolete: test for the 'und' (german)
#   for feature in vectorizer.get_feature_names():
#       if ( feature == 'und' ):
#           print('und')

    X.shape
    return X, labels

def plot_2d(X, labels, labels_color_map):
    """reducing the TFIDF document-term matrix into two dimensions using PCA.
       plotting the 2D (PCA) reduced data by using labels and color map."""
    import matplotlib.pyplot as plt
    from sklearn.decomposition import PCA
    nonsparse_X = X.toarray()
    nonsparse_X.shape
    reduced_X = PCA(n_components=2).fit_transform(nonsparse_X)

    fig, ax = plt.subplots()
    for index, instance in enumerate(reduced_X):
        # print instance, index, labels[index]
        x, y = reduced_X[index]
        color = labels_color_map.get(labels[index]) # [labels[index]]
        ax.scatter(x, y, c=color, marker='D')
        num = labels[index].split('-')[0]
#       print(num)
        ax.annotate(num, xy=(x, y), size=11)

    markers = [plt.Line2D([0,0],[0,0],color=color, marker='D', linestyle='') for color in labels_color_map.values()]
    plt.legend(markers, labels_color_map.keys(), numpoints=1)
    plt.show()

def plot_3d(X, labels, labels_color_map):
    """reducing the TFIDF document-term matrix into three dimensions using PCA.
       plotting the 3D (PCA) reduced data by using labels and color map."""
    import matplotlib.pyplot as plt
    from sklearn.decomposition import PCA
    nonsparse_X = X.toarray()
    nonsparse_X.shape
    reduced_X = PCA(n_components=3).fit_transform(nonsparse_X)

    # fig, ax = plt.subplots(projection='3d')
    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')

    for index, instance in enumerate(reduced_X):
        # print instance, index, labels[index]
        x, y, z = reduced_X[index]
        color = labels_color_map.get(labels[index])
        scatter = ax.scatter(x, y, z, c=color, marker='D', cmap=labels_color_map.values(), label=labels[index])

        num = labels[index].split('-')[0]
#       print(num)

        ax.text(x, y, z, num, size=11)
#        ax.annotate(num, xyz=(x, y, z))
#        scatter = ax.annotate(txt, x, y, z)

    ax.legend(loc='upper left', numpoints=1, ncol=3, fontsize=8, bbox_to_anchor=(0, 0))
    #ax.legend()
    ax.grid(True)
    plt.show()
#    return plt

def sample_rotate_3d():
    """An example 3D rotating plot."""
    from mpl_toolkits.mplot3d import axes3d
    import matplotlib.pyplot as plt

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    # load some test data for demonstration and plot a wireframe
    X, Y, Z = axes3d.get_test_data(0.1)
    ax.plot_wireframe(X, Y, Z, rstride=5, cstride=5)

    # rotate the axes and update
    for angle in range(0, 360):
        ax.view_init(30, angle)
        plt.draw()
        plt.pause(.001)

def rotate_3d(X, labels, labels_color_map):
    """Reducing the TFIDF document-term matrix into three dimensions using PCA.
       Plotting and rotating of the 3D (PCA) reduced data by using labels and color map."""
    from sklearn.decomposition import PCA
    nonsparse_X = X.toarray()
    nonsparse_X.shape
    reduced_X = PCA(n_components=3).fit_transform(nonsparse_X)

    from mpl_toolkits.mplot3d import axes3d
    import matplotlib.pyplot as plt

    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')

    # load some test data for demonstration and plot a wireframe
#   X, Y, Z = axes3d.get_test_data(0.1)
#   ax.plot_wireframe(X, Y, Z, rstride=5, cstride=5)

    for index, instance in enumerate(reduced_X):
        # print instance, index, labels[index]
        x, y, z = reduced_X[index]
        color = labels_color_map.get(labels[index])
        scatter = ax.scatter(x, y, z, c=color, marker='D', cmap=labels_color_map.values(), label=labels[index])

        num = labels[index].split('-')[0]
#       print(num)

        ax.text(x, y, z, num, size=11)
#       ax.annotate(num, xyz=(x, y, z))
#       scatter = ax.annotate(txt, x, y, z)

    ax.legend(loc='upper left', numpoints=1, ncol=3, fontsize=8, bbox_to_anchor=(0, 0))
#   ax.legend()
    ax.grid(True)
#   plt.show()

    rounds = 8
    # rotate the axes and update
    for angle in range(0, 360 * rounds):
        ax.view_init(27, angle)
        plt.draw()
        plt.pause(.001)

def get_random_color():
    """Returns a hexadecimal RGB color (i.e. '#B4DAB5') generated by random module using randint."""
    # see: https://stackoverflow.com/questions/13998901/generating-a-random-hex-color-in-python
    import random
    rgb = lambda : random.randint(0,255)
   
    color = str('#%02X%02X%02X' % (rgb(),rgb(),rgb()))
    return color

def get_label_color_map(labels=['A', 'B', 'C', 'D', 'E']):
    """Generates a label color map (dictionary) for a given
       list of labels (using get_random_color function)."""
    labels_color_map = dict()
    for index in range(len(labels)):
        labels_color_map.setdefault(labels[index], get_random_color())
    return labels_color_map

def main():
    import nltk
    nltk.download('punkt')
    nltk.download('stopwords')

    X, labels = get_X(path_to_docs='./pkgsrc/licenses/*', language='english')
    labels_color_map = get_label_color_map(labels=labels)

    # see: https://matplotlib.org/stable/tutorials/colors/colormaps.html
    print(labels)

    plot_2d(X, labels, labels_color_map)
    plot_3d(X, labels, labels_color_map)
    rotate_3d(X, labels, labels_color_map)

if __name__ == "__main__":
    # execute only if run as a script
    main()
    print(u'Might be necessary to press CTL + C to quit.')
    print(u'Enjoy sliding!')
